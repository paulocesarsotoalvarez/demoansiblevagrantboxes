# **ANSIBLE - Sandbox**  
  
Author: Paulo Cesar Soto Alvarez.  
Version: 1.0.  
Date: November 7th 2018.  
Inspired in: https://www.ansible.com/resources/webinars-training/introduction-to-ansible  

## **Using Roles** 
  
- Create directory and entering on it.
```sh  
$ mkdir roles && cd roles  
```  
  
- Create Roles common and apache with ansible-galaxy.  
```sh  
$ ansible-galaxy init common  
  
$ ansible-galaxy init apache  
```  
  
Our examples require some variables, for this reason we create the group_vars, inside I define some for this example.  
```sh
$ mkdir group_vars  
```  

We focus on the apache role.  

We pass the tasks for apache2 created previously on `my-vagrant.yml` to `roles/apache/tasks/main.yml`.

Now we create three more tasks:  
- create sites directories  
- copy an index.html  
- copy apache conf  
  
The task `create sites directories` uses the variable `apache_dirs` you can locate on the file `group_vars/webservers`.  
  
The task `copy an index.html` use the template `index.html.j2` located in `roles/apache/templates/`, this file `index.html.j2` uses variables defined on the file `group_vars/webservers`.   
    
The last task `copy apache conf` requires to invoke a handler this is defined on file `main.html` located in `roles/apache/handlers/`, this task also use the template `apache.conf-Debian.j2` located in `roles/apache/templates/`.  

Our boxes used on this test has `Ubuntu 18.04` is a `Debian` flavor, for this reason the file template configuration use this name on his definition; if we use a mixed boxes, we need to create many templates as boxes flavors we had.  
