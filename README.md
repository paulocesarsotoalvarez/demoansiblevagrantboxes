# **ANSIBLE - Sandbox**  
  
Author: Paulo Cesar Soto Alvarez.  
Version: 1.0.  
Date: November 7th 2018.  
Inspired in: https://www.ansible.com/resources/webinars-training/introduction-to-ansible  
  

## **Requirements**  
  
- xCode 
  - Developer Tools   
- Ansible installation (on my OSX)  
  ```sh
  $ brew install ansible  
  ```  

## **It's time to play**  
    
- Get Info Setup on local  
  ```sh
  $ ansible localhost -m setup  
  ```  
  - This command display a lot of information on your terminal screen.  
- Now let's create our inventory file, you can name it as you want.  
  ```sh
  $ touch inventory-dtp 
  ``` 
  - See the file inventory-dtp    
- Now let's test using the ping module, for this demo I use the root user.  
  ```sh
  $ ansible all -i inventory-dtp -u root -m ping  

   52.54.247.42 | UNREACHABLE! => {
     "changed": false,
     "msg": "Failed to connect to the host via ssh:  root@52.54.247.42: Permission denied  (publickey).\r\n",
     "unreachable": true
   }
   162.213.195.245 | SUCCESS => {
     "changed": false,
     "ping": "pong"
   }
   181.215.156.236 | SUCCESS => {
     "changed": false,
     "ping": "pong"
   }
  ```   
  - On my enviroment, the webmago group requires a pam key and return us an error on the results.
  
## **Ansible Playground using Vagrant Machines**  
  
- Create Directories for the boxes  
  ```sh  
  $ mkdir -p Vagrants/{box1,box2,box3}  
  ```  
- Initialise vagrant inside Vagrants/box{N}  
  ```sh  
  $ vagrant init --output Vagrants/box1/Vagrantfile  
    
  $ vagrant init --output Vagrants/box2/Vagrantfile  
    
  $ vagrant init --output Vagrants/box3/Vagrantfile  
  ```

- Startup all the boxes  
  ```sh
  $ cd Vagrants/box1 && vagrant up && cd ../../  
    
  $ cd Vagrants/box2 && vagrant up && cd ../../  
    
  $ cd Vagrants/box3 && vagrant up && cd ../../  
  ```  

- Copy my pub key on each node for passwordless login.  
  - Step 1.
  ```sh
  $ cd ~/.ssh/  
  
  $ scp id_rsa.pub vagrant@192.168.33.31:/home/vagrant/.ssh/macbook_webmago_rsa.pub  
  vagrant@192.168.33.31 password: vagrant     
  ```  
   
  - Step 2, repeat on each node.
  ```sh  
  $ ssh vagrant@192.168.33.31  
  vagrant@192.168.33.31s password: vagrant  
    
  vagrant@ansible-node-1:~$ cd .ssh/  
  vagrant@ansible-node-1:~/.ssh$ cat macbook_webmago_rsa.pub >> authorized_keys  
  vagrant@ansible-node-1:~$ exit  
  ```  
    
  - Step 3, Test the passwordlees login on each node.  
  ```sh  
  $ ssh vagrant@192.168.33.33  
  Welcome to Ubuntu 18.04.1 LTS (GNU/Linux 4.15.0-29-generic x86_64)  
  ...  
  Last login: Wed Nov  7 16:41:28 2018 from 192.168.33.1  
  vagrant@ansible-node-3:~$ 
  ```  

- Setup the inventory file.  
  ```sh  
  $ touch hosts-vagrants.yml ## See file on the repo
  ```  
    
- Setup the Playground file.  
  ```sh  
  $ touch my-vagrant.yml  ## See file on the repo
  ```  
  
- Let's play time.
  ```sh  
  $ ansible-playbook -i hosts-vagrants my-vagrant.yml

  PLAY [install and start apache] ****************************************************************************************************************
  
  TASK [Gathering Facts] *************************************************************************************************************************
  ok: [192.168.33.33]
  ok: [192.168.33.31]
  ok: [192.168.33.32]
  
  TASK [install apache] **************************************************************************************************************************
  changed: [192.168.33.33]
  changed: [192.168.33.32]
  changed: [192.168.33.31]
  
  TASK [start apache] ****************************************************************************************************************************
  ok: [192.168.33.31]
  ok: [192.168.33.32]
  ok: [192.168.33.33]
  
  PLAY RECAP *************************************************************************************************************************************
  192.168.33.31              : ok=3    changed=1    unreachable=0    failed=0
  192.168.33.32              : ok=3    changed=1    unreachable=0    failed=0
  192.168.33.33              : ok=3    changed=1    unreachable=0    failed=0
  ```  
  
